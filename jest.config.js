module.exports = {
  collectCoverageFrom: [
    '<rootDir>/src/**/*.{js,jsx}',
    '!<rootDir>/src/*.{js,jsx}',
  ],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/jest.mock-file.js',
    '\\.pcss$': 'identity-obj-proxy',
  },
  moduleFileExtensions: ['js', 'jsx', 'json'],
  testEnvironment: 'node',
  testMatch: ['<rootDir>/tests/**/*.{js,jsx}'],
  transform: {
    '^.+\\.(js|jsx)$': '<rootDir>/node_modules/babel-jest',
  }
};
