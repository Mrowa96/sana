import React from 'react';
import Button from '@/components/Button';
import styles from './styles.pcss';

const onClick = () => {
  alert('Oh.. You clicked me! ');
};

const App = () => (
  <main className={styles.Wrapper}>
    <h1>Sana</h1>
    <Button onClick={onClick}>Click me</Button>
  </main>
);

export default App;
