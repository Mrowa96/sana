import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './styles.pcss';

const Button = ({ children, onClick, className, ...props }) => (
  <button
    data-testid="button"
    {...props}
    type="button"
    className={classnames(styles.Button, className)}
    onClick={onClick}
  >
    {children}
  </button>
);

Button.propTypes = {
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func.isRequired,
  className: PropTypes.string,
};

Button.defaultProps = {
  className: '',
};

export default Button;
