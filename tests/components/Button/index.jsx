import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Button from '../../../src/components/Button';

describe('Button component', () => {
  let component;
  let onClick;

  beforeEach(() => {
    onClick = jest.fn();
    component = <Button onClick={onClick}>Test label</Button>;
  });

  it('should render button with text label', () => {
    const { getByTestId } = render(component);

    expect(getByTestId('button')).toHaveTextContent('Test label');
  });

  it('should trigger onClick method', () => {
    const { getByTestId } = render(component);

    fireEvent.click(getByTestId('button'));

    expect(onClick).toHaveBeenCalledTimes(1);
  });

  it('should have button type', () => {
    const { getByTestId } = render(component);

    expect(getByTestId('button')).toHaveAttribute('type', 'button');
  });
});
